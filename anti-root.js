setTimeout(function() {

  if (!Java.available){
    console.log("\n[x] Can't find Dalvik/ART to hook to. Do Ctrl-C");
    return;
  }
  
  Java.perform(function() {
    console.log("\n[-] Defeating root detection");

    var rootArtifacts = [
      '/su',
      'busybox',
      'superuser',
      'supersu',
      '.su',
      '/amphoras',
      '/su-backup',
      '/bacon'
    ];
    var rootPackages = [
      'com.koushikdutta.rommanager',
      'com.koushikdutta.rommanager.license',
      'com.dimonvideo.luckypatcher',
      'com.chelpus.lackypatch',
      'com.ramdroid.appquarantine',
      'com.ramdroid.appquarantinepro'
    ];

    // Grab a reference to Java's File class
    var File = Java.use("java.io.File");
    console.log("[*] Hooking to java.io.File.exists()");
    // Override the default behaviour of exists()
    File.exists.implementation = function() {
      var fileBeingChecked = this.path.value;
      for (i = 0; i < rootArtifacts.length; i++) {
        if (fileBeingChecked.toLowerCase().indexOf(rootArtifacts[i]) >= 0) {
          //console.log('[*] Intercepted check for "' + fileBeingChecked + '"');
          // Lie to the app by telling it the file doesn't exist
          return false;
        }
      }
      return this.exists();
    };

    // Hide common rooting packages
    var PackageManager = Java.use('android.content.pm.PackageManager');
    console.log("[*] android.content.pm.PackageManager.getPackageInfo()");
    PackageManager.getPackageInfo.implementation = function(arg1, arg2) {
      console.log(arg1, arg2);
      var packageBeingChecked = this.packageName.value;
      for (i = 0; i < rootPackages.length; i++) {
        if (packageBeingChecked === rootArtifacts) {
          console.log('[*] Intercepted check for "' + packageBeingChecked + '"');
          // Lie to the app by telling it the file doesn't exist
          throw PackageManager.NameNotFoundException.$new();
        }
      }
      return this.getPackageInfo(args1, arg2);
    };


  });
},0);
