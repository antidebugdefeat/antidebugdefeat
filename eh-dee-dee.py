#!/usr/bin/env python3

import frida
import time
import argparse
import sys
from colorama import init

from colorama import Fore, Style

init()
ANTI_ROOT_SCRIPT = "anti-root.js"
ANTI_PIN_SCRIPT = "anti-pin.js"

class AntiDebugDefeat:

    def __init__(self, target_app, launch, anti_root, anti_pin):
        self.target_app = target_app
        self.launch = launch
        self.anti_root = anti_root
        self.anti_pin = anti_pin

    def create_frida_session(self):
        if self.launch:
            self.dev = frida.get_usb_device(1)
            self.pid = self.dev.spawn([self.target_app]);
            time.sleep(1)
            self.frida_session = self.dev.attach(self.pid)
        else:
            self.frida_session = frida.get_usb_device(1).attach(self.target_app)

    def resume_process(self):
        self.dev.resume(self.pid)


    def run(self):
        try:
            self.create_frida_session()

        except frida.ProcessNotFoundError:
            print(Fore.RED + "[x] Unable to find", self.target_app)
            print(Style.RESET_ALL)
            sys.exit(1)

        except frida.ServerNotRunningError:
            print(Fore.RED + "[x] Unable to reach Frida server")
            print(Style.RESET_ALL)
            sys.exit(1)

        except frida.TimedOutError:
            print(Fore.RED + "[x] Something went wrong. Timing out...")
            print(Style.RESET_ALL)
            sys.exit(1)


        script_string = ""
        if (self.anti_root):
            script_file = open(ANTI_ROOT_SCRIPT)
            script_string += script_file.read()
            script_file.close()

        if (self.anti_pin):
            script_file = open(ANTI_PIN_SCRIPT)
            script_string += script_file.read()
            script_file.close()

        self.script = self.frida_session.create_script(script_string)

        self.script.load()

        if self.launch:
            self.resume_process()

    def clean(self):
        try:
            print("[-] Cleaning...")
            self.script.unload()
            self.frida_session.detach()

        except frida.InvalidOperationError:
            print(Fore.RED + "[x] Can't clean because target app already dead")
            print(Style.RESET_ALL)


def main():
    parser = argparse.ArgumentParser(description="Anti Root & Pin: A small utility for defeating root detection and certificate pinning in Android apps",
    epilog="Example: ./eh-dee-dee.py -a com.example.app --launch --anti-root --anti-pin")
    parser.add_argument("-a", metavar="app.package.name", type=str, nargs="?", help="Target app. -a com.example.app")
    parser.add_argument("--launch", action='store_true', help="Force launch the app")
    parser.add_argument("--anti-root", action='store_true', help="Attempt defeating root detection")
    parser.add_argument("--anti-pin", action='store_true', help="attempt defeating certificate pinning")
    parsed_args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(2)

    if not parsed_args.a:
        print(Fore.RED + "[x] Please set a target app")
        print(Style.RESET_ALL)
        parser.print_help()
        sys.exit(2)

    if not parsed_args.anti_root and not parsed_args.anti_pin:
        print(Fore.RED + "[x] Please set at least one 'anti' option")
        print(Style.RESET_ALL)
        parser.print_help()
        sys.exit(2)

    eh_dee_dee = AntiDebugDefeat(parsed_args.a, parsed_args.launch, parsed_args.anti_root, parsed_args.anti_pin)
    try:
        eh_dee_dee.run()
        while True:
            pass
    except KeyboardInterrupt:
        print("\n[-] Exiting...")
        eh_dee_dee.clean()


if __name__ == "__main__":
    main()
