setTimeout(function() {

  if (!Java.available){
    console.log("\n[x] Can't find Dalvik/ART to hook to. Do Ctrl-C");
    return;
  }

  Java.perform(function () {
    console.log("\n[-] Defeating certificate pinning");
    var intercetpor_ca = "/data/local/tmp/intercetpor_ca.crt";

    // Grab references to the needed libraries
    var FileInputStream = Java.use("java.io.FileInputStream");
    var BufferedInputStream = Java.use("java.io.BufferedInputStream");
    var SSLContext = Java.use("javax.net.ssl.SSLContext");
    var X509Certificate = Java.use("java.security.cert.X509Certificate");
    var CertificateFactory = Java.use("java.security.cert.CertificateFactory");
    var KeyStore = Java.use("java.security.KeyStore");
    var TrustManagerFactory = Java.use("javax.net.ssl.TrustManagerFactory");

    // Follow a ceritificate-pinning flow similar to legitimate, but use intercetpor_ca
    try {
      var myFileInputStream = FileInputStream.$new(intercetpor_ca);
    } catch(e) {
      console.log("[x] Error loading " + intercetpor_ca + " . Are you sure you have it there?");
      return;
    }

    var myBufferedInputStream = BufferedInputStream.$new(myFileInputStream);
    var myCertificateFactory = CertificateFactory.getInstance("X.509");
  	var caCert = myCertificateFactory.generateCertificate(myBufferedInputStream);
    myBufferedInputStream.close();

    var certInfo = Java.cast(caCert, X509Certificate);
    var keyStoreType = KeyStore.getDefaultType();
    var myKeyStore = KeyStore.getInstance(keyStoreType);
    myKeyStore.load(null, null);
    myKeyStore.setCertificateEntry("ca", caCert);

    // Build our fake TrustManager
    var tmfAlgo = TrustManagerFactory.getDefaultAlgorithm();
    var myTrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgo);
    myTrustManagerFactory.init(myKeyStore);

   	SSLContext.init.overload(
      "[Ljavax.net.ssl.KeyManager;",
      "[Ljavax.net.ssl.TrustManager;",
      "java.security.SecureRandom"
    ).implementation = function(km, tm, random) {
   		console.log("[*] Custom SSLContext initilization detected");

      // Whenever the app builds a TrustManager, hand it our fake one
   		SSLContext.init.overload(
        "[Ljavax.net.ssl.KeyManager;",
        "[Ljavax.net.ssl.TrustManager;",
        "java.security.SecureRandom"
      ).call(this, km, myTrustManagerFactory.getTrustManagers(), random);
   		console.log("[*] Injected fake TrustManager");
   	}

  });
},0);
