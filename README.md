# AntiDebugDefeat #

A small utility that attempts to defeat certificate pinning and root detection in Android apps

### Requirements ###

* Python3
* Frida libs - `pip3 install -r requirements.txt`
* ADB - `sudo apt install android-tools-adb`
* Frida server running on the device.
* For defeating cert-pinning, make sure to have the interceptor cert in `/data/local/tmp/intercetpor_ca.crt`

### Examples ###

* `./eh-dee-dee.py -a com.example.app --anti-root --anti-pin`
* `./eh-dee-dee.py -a com.example.app --launch --anti-root --anti-pin`

For testing purposes, an accompanying test application is included [SuperSecureApp](https://bitbucket.org/antidebugdefeat/supersecureapp/).

* `./eh-dee-dee.py -a com.example.supersecureapp --anti-root`

### Note ###
There are numerous ways to perform root detection and certificate pinning.
This tool attempts to defeat a few of them, and can be used as a basis to extend and add more techniques.

```
user@host:~$ ./eh-dee-dee.py -h
usage: eh-dee-dee.py [-h] [-a [app.package.name]] [--launch] [--anti-root]
                     [--anti-pin]

Anti Root & Pin: A small utility for defeating root detection and certificate
pinning in Android apps

optional arguments:
  -h, --help            show this help message and exit
  -a [app.package.name]
                        Target app. -a com.example.app
  --launch              Force launch the app
  --anti-root           Attempt defeating root detection
  --anti-pin            attempt defeating certificate pinning

Example: ./eh-dee-dee.py -a com.example.app --launch --anti-root --anti-pin
```

56C5848CC7BB6967BF6710004935BE3AB2F651C0ADFCB46855E9B9045BF985C5
